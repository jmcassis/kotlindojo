package com.cit.stanis.kotlintodo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val list = listOf("abc", "teste", "terceiro", "quarto", "quinto")

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, list)
        list_todo.adapter = adapter
    }

}